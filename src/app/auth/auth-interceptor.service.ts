import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

      const authRequest = req.clone({
        setHeaders:{
          'Authorization':`Bearer ${this.auth.getToken()}`
        }
      })

      return next.handle(authRequest)
        .pipe(
          catchError((err, retry) => {
            if(err instanceof HttpErrorResponse && err.statusText == "Unauthorized" ){
              this.auth.authorize()
              // return this.auth.refreshToken().mapTo(retry)
            }
            return Observable.throw(err)
          })
        )
  }

  constructor(private auth:AuthService) {  }
}
