import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppComponent } from './app.component';
// import { MusicModule } from './music/music.module';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { Routing } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { PlaylistsModule } from './playlists/playlists.module';
import { TestingComponent } from './testing.component';

@NgModule({
  declarations: [
    AppComponent,
    // TestingComponent
  ],
  // schemas:[],
  imports: [
    BrowserModule,
    AuthModule,
    HttpClientModule,
    // MusicModule,
    Routing,
    PlaylistsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private auth:AuthService){
    this.auth.getToken()
  }
}
