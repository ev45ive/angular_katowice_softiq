import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service';

@Component({
  selector: 'app-music-search',
  template: `
    <div class="row">
      <div class="col">
        <app-search-form 
        [query]="queries$ | async "
        (queryChange)="search($event)">
        </app-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
      <button (click)="hide = !hide">Toggle</button>

        <app-albums-list [albums]="albums$ | async"
        *ngIf="!hide"></app-albums-list>

      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums$ = this.musicService.getAlbums()
  queries$ = this.musicService.queries$

  constructor(private musicService: MusicService) { }

  search(query) {
    this.musicService.search(query)
  }

  ngOnInit() {

  }

}
