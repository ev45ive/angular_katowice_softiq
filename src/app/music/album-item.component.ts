import { Component, OnInit, Input } from '@angular/core';
import { Album } from './music.interface';
import { MusicService } from './music.service';

// https://getbootstrap.com/docs/4.0/
// components/card/#card-groups

@Component({
  selector: 'app-album-item, [app-album-item]',
  template: `
    <img class="card-img-top" 
          [src]="album.images[0].url">
    <div class="card-body">
      <h5 class="card-title">
        {{album.name}}
      </h5>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album: Album

  constructor() {
  }

  ngOnInit() {
  }

}
