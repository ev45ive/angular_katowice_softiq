import { Component, OnInit, Inject, Input } from '@angular/core';
import { Album } from './music.interface';
import { MusicService } from './music.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-albums-list',
  template: `
  <div class="card-group">
    <div class="card" 
         *ngFor="let album of albums"
         [album]="album"
         app-album-item>
    </div>
  </div>
  `,
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums: Album[]

  constructor() { }

  ngOnInit() {
  }
}
