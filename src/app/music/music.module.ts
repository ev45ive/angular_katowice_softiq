import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { MusicService, API_URL } from './music.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { Routing } from './music.routing';
import { AuthModule } from '../auth/auth.module';

@NgModule({
  imports: [
    CommonModule,
    // HttpClientModule,
    ReactiveFormsModule,
    // AuthModule,
    Routing
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumsListComponent, 
    AlbumItemComponent
  ],
  // exports:[
  //   MusicSearchComponent
  // ],
  providers:[
    {
      provide: API_URL,
      useValue: 'https://api.spotify.com/v1/search'
    },
    // {
    //   provide: HttpClient,
    //   useClass: AuthenticatedHttpClient
    // },
    MusicService
    // {
    //   provide: MusicService,
    //   useClass: MusicService,
    //   // deps: ['api_url']
    // }
    // {
    //   provide: 'musicService',
    //   useFactory: (url) => {
    //     return new MusicService(url)
    //   },
    //   deps: ['api_url']
    // }
  ]
})
export class MusicModule { }
