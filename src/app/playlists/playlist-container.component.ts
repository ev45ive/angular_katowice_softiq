import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-container',
  template: `
    <app-playlist-details [playlist]="playlist$ | async" (playlistChange)="save($event)"></app-playlist-details>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist$ = this.route.params.pipe( 
    switchMap( params => this.playlistsService.getPlaylist(params['id']) 
  ))
  
  constructor(private playlistsService: PlaylistsService, private route:ActivatedRoute) { }
  
  save(p){
    this.playlistsService.savePlaylist(p)
  }

  ngOnInit() {
  }

}
