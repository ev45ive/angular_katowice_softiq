import { TestBed, inject } from '@angular/core/testing';

import { PlaylistsService, DATA_TOKEN } from './playlists.service';

describe('PlaylistsService', () => {

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {
          provide: DATA_TOKEN,
          useValue: [
            {
              id: 123,
              name: 'The best of Angular',
              favourite: false,
              color: '#ff0000'
            },
            {
              id: 234,
              name: 'Angular Greatest Hits',
              favourite: true,
              color: '#00ff00'
            },
            {
              id: 345,
              name: 'Angular TOP20',
              favourite: false,
              color: '#0000ff'
            }
          ]
        },
        PlaylistsService
      ],
      declarations: [],
    });

  });

  it('should be created', inject([PlaylistsService], (service: PlaylistsService) => {
    expect(service).toBeTruthy();
  }));

  it('should get playlists', inject([PlaylistsService, DATA_TOKEN], (service: PlaylistsService, data) => {
    service.getPlaylists().subscribe(playlists => {
      expect(playlists).toEqual(data)
    })
  }))

  it('should get playlist by id', inject([PlaylistsService, DATA_TOKEN], (service: PlaylistsService, data) => {
    const item = data[2]
    service.getPlaylist(item.id).subscribe(playlist => {
      expect(playlist).toEqual(item)
    })
  }))

  it('should update playlist', inject([PlaylistsService, DATA_TOKEN], (service: PlaylistsService, data) => {
    const item = data[2]

    const changed = {
      ...item,
      name: "Testing123"
    }
    service.savePlaylist(changed).subscribe(saved => 
        service.getPlaylist(saved.id).subscribe(updated => {
            expect(updated).toEqual(saved)
        })
    )
  }))


});
