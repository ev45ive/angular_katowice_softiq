import { Component, Input, ViewChild,
  OnInit, 
  AfterViewInit, 
  DoCheck, 
  OnChanges, 
  OnDestroy, 
  AfterContentChecked, 
  AfterContentInit, 
  AfterViewChecked,
  EventEmitter,
  Output
} from '@angular/core';
import { Playlist } from './playlists.interfaces';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-playlist-details',
  template: `
  <ng-container [ngSwitch]="mode" >
    <form *ngSwitchCase=" 'edit' " #playlistForm="ngForm"  (submit)="save(playlistForm)">
      <div class="form-group">
        <label>Name:</label>
        <input type="text" class="form-control" ngModel name="name">
      </div>
      <div class="check-group">
        <label>Favourite</label>
        <input type="checkbox" class="check-control" ngModel name="favourite">
      </div>
      <div class="form-group">
        <label>Color:</label>
        <input type="color" ngModel name="color">
      </div>
      <input class="btn btn-danger" type="button" (click)="cancel()" value="Cancel">
      <input class="btn btn-success" type="submit" value="Save">
    </form>

    <div *ngSwitchDefault>
      <div class="form-group">
        <label>Name:</label>
        <p>{{playlist.name}}</p>
      </div>
      <div class="form-group">
        <label>Favourite:</label>
        <p>{{playlist.favourite? 'Yes':'No'}}</p>
      </div>
      <div class="form-group">
        <label>Color:</label>
        <p class="colored" [ngStyle]="{
          color: playlist.color,
          borderBottomColor: playlist.color
        }">{{playlist.color}}</p>
      </div>
      <button class="btn btn-info" (click)="edit()">Edit</button>
    </div>

  </ng-container>
  `,
  styles: [`
      .colored{
        border-bottom: 1px solid black;
      }
  `]
})
export class PlaylistDetailsComponent implements OnInit, AfterViewInit {

  @Input('playlist')
  set onPlaylist(playlist:Playlist){
    this.cancel()
    this.playlist = playlist
  }

  @ViewChild('playlistForm')
  playlistForm: NgForm

  playlist: Playlist

  mode = 'show'

  @Output()
  playlistChange = new EventEmitter<Playlist>()

  save(form:NgForm){
    this.playlistChange.emit({
      ...this.playlist,
      ...form.value
    })
  }

  cancel(){
    this.mode = 'show'
  }

  edit(){
    this.mode = 'edit'
    // wait for form to render
    setTimeout(()=>{
      this.playlistForm.reset(this.playlist)
    })
  }

  constructor() {
    // console.log('constructor')
  }

  ngOnChanges(changes){
    // console.log(changes)
  }

  ngOnInit() {
    // console.log('ngOnInit',this.playlist)
  }

  ngAfterViewInit(): void {
    // console.log('ngAfterViewInit', this.playlistForm)

    // setTimeout(()=>{
    //   this.playlistForm.reset(this.playlist)
    // })
  }

}
