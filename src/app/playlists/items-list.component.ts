import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Entity, Named } from './playlists.interfaces';
// [style.borderLeftColor]=" (hover == item? item.color : 'inherit') "
// (mouseenter)=" hover = item "
// (mouseleave)=" hover = false "

//     <div class="list-group" selection (selectionChange)=""
//         <div class="list-group-item"  [selectionItem]="item"
//           (selectionChange)="$event"
//           selectionActiveClass="active"

@Component({
  selector: 'app-items-list',
  template: `
    <div class="list-group" >
      <div class="list-group-item"        

          (click)="select(item)"
          [class.active]=" selected?.id == item.id"
       
          [highlight]="item.color"

          *ngFor="let item of items; trackBy: trackFn; let i = index">
          {{i + 1}}. {{item.name}}
      </div>
    </div>
  `,
  styles: [`
    .list-group-item{     
      border-left: 5px solid black;
    }
    :host(.bordered) {
      border:1px solid black;
      display:block;
    }
    :host-context(.colored) ::ng-deep p{
        color: hotpink;
    }
  `]
})
export class ItemsListComponent<T extends Entity & Named> implements OnInit {

  hover

  @Input()
  selected

  @Input()
  items:T

  @Output()
  selectedChange = new EventEmitter<T>()

  select(item:T){
    this.selectedChange.emit(item)
  }

  trackFn(item:T){
    return item.id
  }

  constructor() { }

  ngOnInit() {
  }

}
