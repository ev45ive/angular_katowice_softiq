import { Injectable, InjectionToken, Inject, Optional } from '@angular/core';
import { Playlist } from './playlists.interfaces';
import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators';

export const DATA_TOKEN = new InjectionToken<Playlist[]>('Playlists')

@Injectable()
export class PlaylistsService {

  private playlists:Playlist[] = [
    {
      id: 123,
      name: 'The best of Angular',
      favourite: false,
      color: '#ff0000'
    },
    {
      id: 234,
      name: 'Angular Greatest Hits',
      favourite: true,
      color: '#00ff00'
    },
    {
      id: 345,
      name: 'Angular TOP20',
      favourite: false,
      color: '#0000ff'
    }
  ]

  constructor( @Optional() @Inject(DATA_TOKEN) playlists:Playlist[]) { }

  getPlaylists(){
    return of(this.playlists)
  }

  getPlaylist(id:number){
    return of(this.playlists.find(
      playlist => playlist.id == id
    ))
  }

  savePlaylist(playlist){
    const foundIndex = this.playlists.findIndex(p => p.id == playlist.id)
    this.playlists.splice(foundIndex,1,playlist)
    return of(playlist)
  }

}
