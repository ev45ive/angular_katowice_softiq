export interface Entity {
    id: number;
}
export interface Named {
    name: string;
}

export interface Track extends Entity, Named { }

export interface Playlist extends Entity, Named {
    favourite: boolean;
    color: string;
    /** 
     * Playlist tracks
    */
    tracks?: Track[] // Array<Track>
}